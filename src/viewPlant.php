<!DOCTYPE html>
<html>
<head>
	<link href="style/default.css" rel="stylesheet" type="text/css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<style>
		tr {
			font-size:1.5em;
		}
		tr:hover {
			background:lightgray;
		}
	</style>
	<script>
		$(document).ready(function() {
			$("#edit").on("click", function() {
				var plantId = '<?php echo $_GET["plantID"]?>';
				url = 'editPlant.php?plantID=' + plantId;
				window.location = url;
			});
			$("#delete").on("click", function() {
				dialog.dialog("open");
			});
		});
	</script>
</head>
<body>
	<h1>Plant information</h1>
<?php
	include 'config.php';
	$plantId = $_GET["plantID"];
	$stmt = mysqli_prepare($mysqli, "SELECT sci_name, family, genus FROM test.plants WHERE plant_id = ?");
	$stmt->bind_param('i', $plantId);
	$stmt->execute();
	$stmt->bind_result($sciName, $family, $genus);
	$stmt->fetch();
	$stmt->close();
	$stmt = mysqli_prepare($mysqli, "SELECT name FROM test.common_names WHERE plant_id = ?");
	$stmt->bind_param('i', $plantId);
	$stmt->execute();
	$results = $stmt->get_result();
	$stmt->close();
	$stmt2 = mysqli_prepare($mysqli, "SELECT c.title, pc.value, pc.category_id, c.type FROM test.categories c INNER JOIN test.plantcategories pc ON c.category_id = pc.category_id WHERE pc.plant_id = ?");
	$stmt2->bind_param('i', $plantId);
	$stmt2->execute();
	$stmt2->bind_result($title, $value, $categoryId, $displayType);
	$display = '<table>' 
		. '<tr>'
		. '<th>Scientific Name</th>'
		. '<th>' . $sciName . '</th>'
		. '</tr>'
		. '<tr>'
		. '<th>Family</th>'
		. '<th>' . $family . '</th>'
		. '</tr>'
		. '<tr>'
		. '<th>Genus</th>'
		. '<th>' . $genus . '</th>';
		$i = 1;
		while ($row = $results->fetch_array(MYSQLI_NUM))
		{
			if ($i == 1) 
			{
				$display = $display . '<tr>'
					. '<th>Common Name(s)</th>'
					. '<th>' . $row[0] . '</th>'
					. '</tr>';
			}
			else 
			{
				$display = $display . '<tr>'
					. '<th></th>'
					. '<th>' . $row[0] . '</th>'
					. '</tr>';
			}
			$i++;
		}
		$i = 1;
		while ($stmt2->fetch())
		{	
			if ($i == 1 || $prevTitle != $title)
			{
				$display = $display . '<tr>'
					. '<th>' . $title . '</th>'
					. '<th>' . $value . '</th>';
				$display = $display . '</tr>';
			}
			else
			{
					$display = $display . '<tr>'
					. '<th></th>'
					. '<th>' . $value . '</th>'
					. '</tr>';
			}
			$prevTitle = $title;
		}
		$stmt2->close();
		$stmt = mysqli_prepare($mysqli, "SELECT DISTINCT location, description FROM test.pictures WHERE plant_id = ?");
		$stmt->bind_param('i', $plantId);
		$stmt->execute();
		$results = $stmt->get_result();
		$stmt->close();
		$display = $display . '<tr>';
		while ($image = $results->fetch_array(MYSQLI_NUM))
		{
				$display .= '<th><table><tr><th><img src=' . $image[0] . ' height="120" /></th><tr><th><label style="font-size:0.5em">' . $image[1] . '</label></th></table></th>';
		}
		$display = $display . '</tr>';
		$display = $display . '</table>';
	echo $display;
?>
<button id="edit">Edit Plant</button>
</body>
</html>