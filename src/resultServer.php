<?php
require_once "config.php";
require_once "classes/Plant.class.php";
// Prevent sending anything to people that are just loading the page
if($_SERVER['REQUEST_METHOD'] != "POST" && $_SERVER['REQUEST_METHOD'] != "GET")
{
    die();
}

// -- Globals -- //
$CATEGORY_TAG = "categoryFilterForm-";

// NOTE: The "Unknown error." is here just for debugging, make sure to remove it in production
$result = array("status"=>"error", "error"=>"Unknown error.");

if($_SERVER['REQUEST_METHOD'] == "POST")
{
    switch($_POST['action'])
    {
        
    }
}
else if($_SERVER['REQUEST_METHOD'] == "GET")
{
    switch($_GET['action'])
    {
        case "updateResults":
            $result["status"] = "ok";
            
            // Prepare the query (NOTE: It doesn't do partial matching in the value portion!)
            $prepStmt = $mysqli->prepare("SELECT plant_id FROM plantcategories WHERE category_id=? AND `value`=?");
            $plants = array();
            
            foreach($_GET as $key => $value)
            {
                // Make sure that we don't check values we don't need to
                if(strpos($key, $CATEGORY_TAG) === false)
                {
                    continue;
                }
                
                $categoryID = substr($key, strlen($key)-1 /* substr() is 0-based */);

                $prepStmt->bind_param("is", $categoryID, $value);
                $prepStmt->execute();
                
                $res = $prepStmt->get_result();

                if($res->num_rows > 0)
                {
                    $val = $res->fetch_assoc();
                    $plants[] = $val['plant_id'];
                }
                
                $prepStmt->reset();
            }
            
            $prepStmt->close();

            // TODO: Put this somewhere else so it can be used by more than just this
            $plantStmt = $mysqli->prepare("SELECT sci_name FROM plants WHERE plant_id=?");
            $nameStmt = $mysqli->prepare("SELECT name FROM common_names WHERE plant_id=?");
            $picStmt = $mysqli->prepare("SELECT * FROM pictures WHERE plant_id=?");
            
            // Make the result array
            $result['value'] = array();
            
            foreach($plants as $p)
            {
                // TODO: This should be merged with the above loop
                $plantStmt->bind_param("i", $p);
                $plantStmt->execute();
                $plantRow = $plantStmt->get_result()->fetch_assoc();
                
                $nameStmt->bind_param("i", $p);
                $nameStmt->execute();
                $nameRes = $nameStmt->get_result();
                $names = array();
                while($nameRow = $nameRes->fetch_assoc())
                {
                    $names[] = $nameRow['name'];
                }
                
                
                $picStmt->bind_param("i", $p);
                $picStmt->execute();
                $picRes = $picStmt->get_result();
                $pics = array();
                while($picRow = $picRes->fetch_assoc())
                {
                    $pics[] = $picRow['location'];
                }
                
                $result["value"][] = array(
                    "id" => $p,
                    "scientificName" => $plantRow['sci_name'],
                    "commonName" => $names,
                    "pictures" => $pics
                );
            }
            break;
        case "getCategories":
            $result["status"] = "ok";
            
            $res = $mysqli->query("SELECT category_id AS id, title FROM categories");
            if($res == false)
            {
                $result["message"] = "Invalid query: ".$mysqli->error;
                // TODO: Force error state and skip out
            }
            
            while($row = $res->fetch_assoc())
            {
                $result['value'][] = array("id" => $row['id'], "name" => $row['title']);
            }
            
            $res->free();
            break;
        case "getCategoryFilter":
            $result["status"] = "ok";
            
            // Get the category
            $stmt = $mysqli->prepare("SELECT * FROM categories WHERE category_id = ? LIMIT 1");
            $stmt->bind_param("i", $_GET['id']);
            $stmt->execute();
            $res = $stmt->get_result();
            
            // Get the options
            $stmt = $mysqli->prepare("SELECT * FROM category_options WHERE category_id = ?");
            $stmt->bind_param("i", $_GET['id']);
            $stmt->execute();
            $optionRes = $stmt->get_result();
            
            $row = $res->fetch_assoc();
            // TODO: Check for failure
            
            $result["value"] = array(
                "id" => $row['category_id'],
                "name" => $row['title'],
                "text" => $row['description'],
                "type" => $row['type'],
            );
            // Only give options if there are some to give
            if($optionRes->num_rows >= 1)
            {
                while($row = $optionRes->fetch_assoc())
                {
                    $result["value"]["options"][] = array($row['id'], $row['options']);
                }
            }
            break;
            case "getSearchByNameResults":
				$result["status"] = "ok";
				$searchText = str_replace("_", " ", $_GET['searchText']);
				$search = '%' . $searchText . '%';
				$stmt = mysqli_prepare($mysqli, "(SELECT plant_id"
												.	" FROM plants"
												.	" WHERE sci_name LIKE ?)"
												.	" UNION"
												.	" (SELECT plant_id"
												.	" FROM common_names"
												.	" WHERE name LIKE ?)");
				$stmt->bind_param('ss', $search, $search);
				$stmt->execute();
				$results = $stmt->get_result();
				$stmt->close();
				$result["value"] = array();
				while ($row = $results->fetch_array(MYSQLI_NUM))
				{
					$plant = new Plant($row[0]);
					$pictures = array();
					foreach ($plant->images as $image)
					{
						$pictures[] = $image[0];
					}
					$result["value"][] = array(
			                    "id"=> $plant->plantId,
			                    "familyName" => $plant->family,
			                    "genusName" => $plant->genus,
			                    "speciesName" => "Species Name",
                                "scientificName" => $plant->scientificName,
			                    "commonName" => $plant->commonNames,
			                    "pictures" => $pictures);
				}
            break;
    }
}

echo json_encode($result);
die();
?>
