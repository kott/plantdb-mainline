<!DOCTYPE html>
<html>
<head>
	<link href="style/default.css" rel="stylesheet" type="text/css" />
	<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
  	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
	<style>
		tr {
			font-size:1.5em;
		}
		tr:hover {
			background:lightgray;
		}
	</style>
	<script>
		$(document).ready(function() {
			var plantId = '<?php echo $_GET["plantID"] ?>';
			var imageLocation;
			var dialog = $("#dialog-confirm").dialog({
            	resizable: false,
            	autoOpen: false,
            	height:140,
            	modal: true,
            	buttons: {
                		"Delete Picture": function() {
                			$.post("deleteImage.php", {location: imageLocation, plantID: plantId});
                			location.reload();
                    		$(this).dialog("close");
                		},
               		 	Cancel: function() {
                    		$(this).dialog("close");
                		}
            		}
        		});
        		var plantDeleteDialog = $("#plantDelete").dialog({
	            	resizable: false,
	            	autoOpen: false,
	            	height:140,
	            	modal: true,
	            	buttons: {
	                		"Delete Plant": function() {
	                			$.post("deletePlant.php", {plantID: plantId});
	                			location.reload();
	                    		$(this).dialog("close");
	                    		url = 'http://plantdb.barthlesys.net/src/index.html';
	                    		window.location = url;
	                		},
	               		 	Cancel: function() {
	                    		$(this).dialog("close");
	                		}
	            		}
        		});

			$(".add").on("click", function() {
				var row = $(this).closest("tr");
				var name = $(this).parent().prev().children("input").attr("name");
				var newRow = jQuery('<tr><th></th><th><input type="text" name="' + name + '"></th></tr>');
				(newRow).insertAfter(row);
			});
			$("#save").on("click", function() {
				$("form").submit();
			});
			$("img").on("click", function() {
				imageLocation = $(this).attr('src');
				dialog.dialog('open');
			});
			$("#delete").on("click", function() {
				plantDeleteDialog.dialog("open");
			});
		});
	</script>
</head>
<body>
	<form action="addValue.php" method="post" enctype="multipart/form-data">
<?php
	include 'config.php';
	include 'classes/Plant.class.php';
	$plantId = $_GET["plantID"];
	$url = 'addValue.php?PlantID=' . $plantId;
	$plant = new Plant($plantId);
	$display = '<input type="hidden" name="plantID" value="' . $plant->plantId . '">';
	$display .= '<table>' 
		. '<tr>'
		. '<th>Scientific Name</th>'
		. '<th><input type="text" name="scientificName" value="' . $plant->scientificName . '"></th>'
		. '</tr>'
		. '<tr>'
 		. '<th>Family</th>'
 		. '<th><input type="text" name="family" value="' . $plant->family . '"></th>'
		. '</tr>'
		. '<tr>'
		. '<th>Genus</th>'
		. '<th><input type="text" name="genus" value="' . $plant->genus . '"></th></tr>';
	$i = 1;
	$display .= '<tr><th>Common Name(s)</th>';
	foreach ($plant->commonNames as $name)
	{
		if ($i == 1)
		{
			$display .= '<th><input type="text" name="commonNames[]" value="' . $name . '"></th>';
			$display .= '<th><button type="button" class="add">Add a common name</button></tr>';
		}
		else 
		{
			$display .= '<tr><th></th><th><input type="text" name="commonNames[]" value="' . $name . '"></th>';
		}
		$i++;
	}
	$numCategories = 1;
	foreach ($plant->categories as $category)
	{	
		$display .= '<tr><th>' . $category->title . '</th>';
		$display .= '<input type="hidden" name="categoryId' . $numCategories . '" value="' . $category->id . '">';
		switch ($category->type) {
			case 'check box':
				if (empty($category->values) && !empty($category->options))
				{
					$i = 1;	
					foreach ($category->options as $option)
					{
						if ($i == 1)
						{
							$display .= '<th><input type="checkbox" name="category' . $numCategories . '[]" class="category" value="' . $option . '">' . $option . '</th></tr>';
						}
						else
						{
							$display .= '<tr><th></th><th><input type="checkbox" name="category' . $numCategories . '[]" class="category" value="' . $option . '">' . $option . '</th></tr>';
						}
						$i++;
					}
				}
				else if (empty($category->values) && empty($category->options))
				{
					$display .= '<th><input type="text" name="category' . $numCategories . '[]" class="category"></th><th><button type="button" class="add">Add a ' . $category->title . '</button></tr>';
				}
				else if (!empty($category->values) && !empty($category->options))
				{
					$i = 1;	
					foreach ($category->options as $option)
					{
						$checked = false;	
						foreach ($category->values as $value)
						{
							if ($value == $option)
							{
								$checked = true;
							}
						}	
						if ($i == 1)
						{
							if ($checked)
							{
								$display .= '<th><input type="checkbox" name="category' . $numCategories . '[]" checked="checked" value="' . $option . '" class="category">' . $option . '</th></tr>';
							}
							else
							{
								$display .= '<th><input type="checkbox" name="category' . $numCategories . '[]" value="' . $option . '" class="category">' . $option . '</th></tr>';
							}
						}
						else 
						{	
							if ($checked)
							{
								$display .= '<tr><th></th><th><input type="checkbox" name="category' . $numCategories . '[]" checked="checked" value="' . $option . '" class="category">' . $option . '</th></tr>';
							}
							else
							{
								$display .= '<tr><th></th><th><input type="checkbox" name="category' . $numCategories . '[]" value="' . $option . '" class="category">' . $option . '</th></tr>';
							}
						}
						$i++;
					}
				}
				else 
				{
					$i = 1;	
					foreach ($category->values as $value)
					{
						if ($i == 1)
						{
							$display .= '<th><input type="text" name="category' . $numCategories . '[]" value="' . $value . '" class="category"></th>';
							$display .= '<th><button type="button" class="add">Add a ' . $category->title . '</button></th></tr>';
						}
						else 
						{
							$display .= '<tr><th></th><th><input type="text" name="category' . $numCategories . '[]" value="' . $value . '" class="category"></th></tr>';
						}
						$i++;
					}
				}
				break;
			case 'drop down':
				if (empty($category->values) && !empty($category->options))
				{
					$display .= '<th><select name="category' . $numCategories . '[]" class="category">';
					foreach ($category->options as $option)
					{
						$display .= '<option value="' . $option . '">' . $option . '</option>';
					}
					$display .= '</select></th></tr>';
				}
				else
				{
					$display .= '<th><select name="category' . $numCategories . '[]" class="category">';
					foreach ($category->options as $option)
					{
						if ($option == $category->values[0])
						{
							$display .= '<option value="' . $option . '" selected="selected">' . $option . '</option>';
						}
						else 
						{
							$display .= '<option value="' . $option . '">' . $option . '</option>';
						}
					}
					$display .= '</select></th></tr>';
				}
				break;
			case 'text':
				if(empty($category->values))
				{
					$display .= '<th><input type="text" name="category' . $numCategories . '[]" class="category"></th></tr>';
				}
				else 
				{
					$display .= '<th><input type="text" name="category' . $numCategories . '[]" class="category" value="' . $category->values[0] . '"></th></tr>';
				}
				break;
		}
		$numCategories++;
	}
	$display .= '<tr>';
	$display .= '<input type="hidden" name="numCategories" value="' . $numCategories . '">';
		foreach ($plant->images as $image)
		{
			$display .= '<th><table><tr><th><img src=' . $image[0] . ' height="120" /></th><tr><th><label style="font-size:0.5em">' . $image[1] . '</label></th></table></th>';
		}
		$display .= '</tr>';
	$display .= '</table>';
	$display .= 'Notes: <br>'
		. '<textarea name="Notes" cols=40 rows=6>' . $plant->notes . '</textarea><br>';
	$display .= '<label>Upload picture: </label> <input type="file" name="file" id="file" /><br><br>';
	$display .= '<label>Photo Description: </label> <input type="text" name="description"><br>';
	$display .= '<button type="button" id="save">Save Changes</button>';
	$display .= '<button type="button" id="delete">Delete Plant</button>';
	echo $display;
?>
</form>
<div id="dialog-confirm" style="display:none" title="Delete this picture?">
	<p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>This picture will be deleted permenantly and will not be recoverable. Are you sure?</p>
</div>
<div id="plantDelete" style="display:none" title="Delete this plant?">
	<p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>This plant will be deleted permenantly and will not be recoverable. Are you sure?</p>
</div>
</body>
</html>