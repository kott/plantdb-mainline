<?php
	include 'config.php';
	$plantId = $_REQUEST["plantID"];
	$stmt = mysqli_prepare($mysqli, "DELETE FROM plants WHERE plant_id = ?");
	$stmt->bind_param('i', $plantId);
	$stmt->execute();
	$stmt->close();
?>