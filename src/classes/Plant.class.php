<?php
require_once '../config.php';

class Category {
    
    private $mysqli;

    public $id;
    public $title;
    public $type;
    public $description;
    public $values;
    public $options;

    public function __construct($categoryId, $plantId) {
        global $mysqli;
        $this->mysqli = $mysqli;
        
        $this->id = $categoryId;
        $stmt2 = $mysqli->prepare("SELECT DISTINCT c.title, c.type, c.description FROM test.categories c WHERE c.category_id = ?");
        $stmt2->bind_param('i', $categoryId);
        $stmt2->execute();
        $stmt2->bind_result($Title, $Type, $Description);
        $stmt2->fetch();
        $stmt2->close();
        $this->title = $Title;
        $this->type = $Type;
        $this->description = $Description;
        $stmt = $mysqli->prepare("SELECT value FROM test.plantcategories WHERE category_id = ? AND plant_id = ?");
        $stmt->bind_param('ii', $categoryId, $plantId);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        $stmt = $mysqli->prepare("SELECT options FROM test.category_options WHERE category_id = ?");
        $stmt->bind_param('i', $categoryId);
        $stmt->execute();
        $hasOptions = $stmt->get_result();
        $stmt->close();
        $this->values = array();
        while ($row = $result->fetch_array(MYSQLI_NUM)) {
            $this->values[] = $row[0];
        }
        $this->options = array();
        while ($row = $hasOptions->fetch_array(MYSQLI_NUM)) {
            $this->options[] = $row[0];
        }
    }

}

class Plant {
    
    private $mysqli;

    public $plantId;
    public $scientificName;
    public $family;
    public $genus;
    public $notes;
    public $commonNames;
    public $categories;
    public $images;

    public function __construct($Id) {
        global $mysqli;
        $this->mysqli = $mysqli;
        
        $this->plantId = $Id;
        $stmt = $mysqli->prepare("SELECT sci_name, family, genus, notes FROM test.plants WHERE plant_id = ?");
        $stmt->bind_param('i', $Id);
        $stmt->execute();
        $stmt->bind_result($sciName, $Family, $Genus, $Notes);
        $stmt->fetch();
        $stmt->close();
        $this->scientificName = $sciName;
        $this->family = $Family;
        $this->genus = $Genus;
        $this->notes = $Notes;
        $stmt = $mysqli->prepare("SELECT name FROM test.common_names WHERE plant_id = ?");
        $stmt->bind_param('i', $Id);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        $this->commonNames = array();
        while ($row = $result->fetch_array(MYSQLI_NUM)) {
            $this->commonNames[] = $row[0];
        }
        $stmt = $mysqli->query("SELECT DISTINCT category_id, title FROM test.categories");
        $categories = $stmt->fetch_all(MYSQLI_NUM);
        $this->categories = array();
        foreach ($categories as $category) {
            $title = $category[1];
            $categoryId = $category[0];
            $this->categories[] = new Category($categoryId, $Id);
        }
        $stmt = $mysqli->prepare("SELECT DISTINCT location, description FROM test.pictures WHERE plant_id = ?");
        $stmt->bind_param('i', $Id);
        $stmt->execute();
        $results = $stmt->get_result();
        $stmt->close();
        $this->images = array();
        while ($row = $results->fetch_array(MYSQLI_NUM)) {
            $this->images[] = $row;
        }
    }

}

?>