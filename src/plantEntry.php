<!DOCTYPE html>
<html>
<head>
	<link href="style/default.css" rel="stylesheet" type="text/css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<title>Add a new plant</title>
	<script>
		//On click, add an input for a common name
		var $i = 0;
		$(document).ready(function() {
			$i++;
			$("#addName").on("click", function() {
				$("<input type=\"text\" />").attr("name", "commonNames[]")
				.appendTo("#commonNames");
				$("<label> X </label>").attr("class", "remove")
				.appendTo("#commonNames");
				$("#commonNames").append(" <br>");
			});
			$("div").on("click", ".remove", function() {
				$(this).prev().remove();
				$(this).next().remove();
				$(this).remove();
			});	
			$(".addAttr").on("click", function() {
				var name = $(this).attr("name");
				$("<input type=\"text\" />").attr("name", "category" + name + "[]")
				.appendTo("#categoryDisplay" + name);
				$("<label> X </label>").attr("class", "remove")
				.appendTo("#categoryDisplay" + name);
				$("#categoryDisplay" + name).append(" <br>");
			});
		});
	</script>
	<style>
		label {
			display:inline-block;
			width:15%;
		}
		div {
			margin-left:15%;
		}
	</style>
</head>
<body>
	<form action="savePlantInfo.php" method="post" enctype="multipart/form-data">
    <a href="index.html">&lt;Go Back to Main Page</a>
		<h1>Add a New Plant</h1>
	<label>Scientific Name: </label><input type="text" name="scientificName"><br>
	<h3 id="addName">Add a common name</h3>
	<div id="commonNames">
	</div>
	<label>Family: </label><input type="text" name="family"><br>
	<label>Genus: </label><input type="text" name="genus"><br><br>
	<label>Upload picture: </label> <input type="file" name="file" id="file" /><br><br>
	<label>Photo Description: </label><input type="text" name="description"><br>
	Notes:<br>
	<textarea name="Notes" cols=40 rows=6></textarea><br><br>
	<h3>Plant Attributes</h3>
	<?php
		include 'config.php';
		$results = mysqli_query($mysqli, "SELECT title, category_id, type FROM categories");
		$categories = $results->fetch_all(MYSQL_NUM);
		$display = "";
		$numCategories = 0;
		foreach ($categories as $row)
		{
			$numCategories++;
			$hasOptions = mysqli_query($mysqli, "SELECT options FROM test.category_options WHERE category_id = " . $row[1]);
			switch ($row[2])
			{
				case "drop down":
					if (mysqli_num_rows($hasOptions))
					{
						$options = $hasOptions->fetch_all(MYSQL_NUM);
						$display = $display . "<label>" . $row[0] . ": </label>" . "<select name=\"category" . $numCategories . "\">";
						foreach ($options as $option)
						{
							$display = $display . "<option value=\"" . $option[0] . "\">" . $option[0] . "</option>";
						}
						$display = $display . "</select><br>";
					}
					else 
					{
						$display = $display . "<label>" . $row[0] . ": </label>" 
						. "<input type=\"text\" name=\"category" . $numCategories . "\"><br>";
						break;
					}
					break;
				case "check box":
					if (mysqli_num_rows($hasOptions))
					{
						$options = $hasOptions->fetch_all(MYSQL_NUM);
						$display = $display . "<label>" . $row[0] . ": </label><br>";
						$display = $display . "<div>";
						foreach ($options as $option)
						{
							$display = $display . "<input type=\"checkbox\" name=\"category" . $numCategories . "[]\" value=\"" . $option[0] . "\">" . $option[0] . "<br>";
						}
						$display = $display . "</div><br>";
					}
					else {
						$display = $display . "<h3 id=\"addAttr" . $numCategories . "\" name=\"" . $numCategories . "\" class=\"addAttr\">Add a " . $row[0] . " </h3>"
						. "<div id=\"categoryDisplay" . $numCategories . "\"></div>";
					}
					break;
				default:
					$display = $display . "<label>" . $row[0] . ": </label>" 
					. "<input type=\"text\" name=\"category" . $numCategories . "\"><br>";
					break;
			}
			$display = $display . "<input type=\"hidden\" name=\"categoryId" . $numCategories . "\" value=\"" . $row[1] . "\">";
		}
		$display = $display . "<br>"; 
		echo $display;
		echo "<input type=\"hidden\" name=\"numCategories\" value=\"" . $numCategories . "\">";
	?>
	<input type="submit" value="Submit">
	</form>
</body>
</html>