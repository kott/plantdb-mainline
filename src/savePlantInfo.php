<?php
	include 'config.php';
	//Get values from text boxes in plantEntry.php
	$sciName = $_POST["scientificName"];
	$family = $_POST["family"];
	$genus = $_POST["genus"];
	$notes = $_POST["Notes"];
	$desc = $_POST["description"];
	$plantNames = $_POST["commonNames"];
	$numCategory = $_POST["numCategories"];
	
	$stmt = $mysqli->prepare("SELECT * FROM plants WHERE sci_name = ?");
	$stmt->bind_param('s', $sciName);
	$stmt->execute();
	$nameExists = $stmt->get_result();
	$stmt->close();
	//Don't allow duplicate scientific names to be added to the database
	if ($nameExists->num_rows > 0)
	{
		header('Location: index.html');
	}
	else 
	{
		$stmt = mysqli_prepare($mysqli, "INSERT INTO plants (sci_name, family, genus, notes) VALUES (?,?,?,?)");
		$stmt->bind_param('ssss', $sciName, $family, $genus, $notes);
		$stmt->execute();
		$plantId = $mysqli->insert_id;
		
		//Store common names in db
		foreach ($plantNames as $name)
		{
			$stmt = mysqli_prepare($mysqli, "INSERT INTO test.common_names (name, plant_id) VALUES(?,?)");
			$stmt->bind_param('si', $name, $plantId);
			$stmt->execute();
		}
		
		for ($i = 1; $i <= $numCategory; $i++ )
		{
			$categoryValue = $_POST["category$i"];
			$categoryId = $_POST["categoryId$i"];
			if (!empty($categoryValue))
			{
				if (is_array($categoryValue))	
				{
					foreach ($categoryValue as $value)
					{
						$stmt = mysqli_prepare($mysqli, "INSERT INTO plantcategories (plant_id, category_id, value) VALUES(?,?,?)");
						$stmt->bind_param('iis', $plantId, $categoryId, $value);
						$stmt->execute();
					}
				}
				else 
				{
					$stmt = mysqli_prepare($mysqli, "INSERT INTO plantcategories (plant_id, category_id, value) VALUES(?,?,?)");
					$stmt->bind_param('iis', $plantId, $categoryId, $categoryValue);
					$stmt->execute();
				}
			}
		}
		
		if (($_FILES["file"]["type"] == "image/gif")
			|| ($_FILES["file"]["type"] == "image/jpeg")
			|| ($_FILES["file"]["type"] == "image/png")
			|| ($_FILES["file"]["type"] == "image/pjpeg"))
		{
			if ($_FILES["file"]["error"] > 0)
			{
				echo "Error uploading file: " . $_FILES["file"]["error"] . "<br/>";
			}
			else
			{
				if (file_exists("/photos/". $_FILES["file"]["name"]))
				{
					echo $_FILES["file"]["name"] . " already exists. ";
				}
				else
				{
					//store picture on server in pictures folder	
					move_uploaded_file($_FILES["file"]["tmp_name"], "C:/inetpub/wwwroot/plantdb/photos/". $_FILES["file"]["name"]);
					//store picture location in pictures table
					$pictureLoc = "/photos/" . $_FILES["file"]["name"];
					$stmt = mysqli_prepare($mysqli, "INSERT INTO pictures (plant_id, location, description) VALUES(?,?,?)");
					$stmt->bind_param('iss', $plantId, $pictureLoc, $desc);
					$stmt->execute();
				}
			}
		}
		else 
		{
			echo "Invalid file type.";
		}
		header('Location: index.html');
	}
?>
