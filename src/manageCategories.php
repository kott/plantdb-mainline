<!DOCTYPE html>
<head>
  <title>PlantDb - Categories</title>
  <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
  <script type="text/javascript" src="js/jquery.form.js"></script>
  <script type="text/javascript" src="js/js_aab.js"></script>
  <link href="style/default.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <h1>Add/Edit Plant Categories</h1>
    <a href="index.html"><- Back to Index</a><br />
    <br />

<form id="Form" action="processCategories.php" method="post">
	<div id="show">
	<?php include("processCategories.php"); ?>
        </div>
        <br />
		<div>
			<label>Category Title:</label>
			<input type="input" name="title" maxlength="20" /><br />
                        <br />
                        <label>Category Type:</label>
                        <select name="type" onchange="if(this.value!='text'){this.form['options'].style.visibility='visible'}else {this.form['options'].style.visibility='hidden'};">
                            <option value="text">Text Field</option>
                            <option value="check box">Check Box</option>
                            <option value="drop down">Drop Down</option>
                        </select><br />
                        <br />
                        <textarea name="options" rows="6" cols="40" style="visibility: hidden;">
Category Options: Clear this box and put in your checkbox/dropdown options here, one per line OR comma delimited.
                        </textarea><br />
                        <br />
                        <label>Category Description:</label>
                        <textarea name="description" rows="6" cols="40">
                        </textarea>
		</div>
		<div>
			<input type="submit" value="Submit" />
		</div>
</form>


</body>
</html>