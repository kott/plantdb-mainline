  $(document).ready(function() {
  $('#Form').ajaxForm({
      target: '#show',
      success: function() {
        $('#show').fadeIn();
      }
    });
  });
  
  function DeleteRow() 
  {
       var parent = $(this).closest('tr');
        $.ajax({
            type: 'post',
            url: 'manageCategories.php', 
            data: 'ajax=1&delete=' + $(this).attr('id'),
            beforeSend: function() {
                parent.animate({'backgroundColor':'#fb6c6c'},300);
            },
            success: function() {
                parent.fadeOut(300,function() {
                    parent.remove();
                    //window.location.reload(true);
                });
            }
        });
        return false;
  }
  
   function UpdateRow()
  {
      var parent = $(this).closest('tr')
      $.ajax({
       type: "post", //post
       url: "manageCategories.php", //data.php?
       data: 'ajax=1&update=' + $(this).attr('id') + '&category=' + parent.find('.category').val() + '&description=' + parent.find('.description').val(),
       cache: false,
       success: function(response)
       {
         alert("Record successfully updated");
       }
     });
 }
 
 
  // Function calls in document.ready
  $(document).ready(function() {
      $('.delete').click(DeleteRow);
      
      $('.update').click(UpdateRow);
  });

