// -- Globals -- //
var resultEntries = new Array();

// This is the categories in the filter accordian, not the ones in the sidebar
var categoryEntries = new Array();
var serverURL = "resultServer.php";

// -- Utility Functions -- //
/**
 * Returns false if the results variable doesn't return an ok result
 */
function verifyResults(results) {
    if(typeof results == "undefined") {
        return false;
    } else if(results == null) {
        return false;
    } else if(results.status == null) {
        return false;
    } else if(results.status != "ok") {
        // If the results aren't ok, display the error if it exists
        if(results.error != null) {
            alert("ERROR: "+results.error);
        } else {
            alert("ERROR: Unable to get an error message. Invalid or errored results.");
        }
        return false;
    }

    return true;
}

// -- Category Functions -- //
/**
 * Fills in the category list in the sidebar
 */
function populateCategoryList() {
    $.get(serverURL,
        {"action":"getCategories"},
        function(result) {
            if(!verifyResults(result))
               return;
           if(result.value == null) {
               alert("ERROR: No results.");
           }
           
           var categories = result.value;
           
           var html = "";
           for(var i = 0; i < categories.length; i++) {
               var row = categories[i];
               html += "<li><a href='#' onClick='toggleCategory("+row.id+"); return false;' id='category-entry-"+row.id+"'>"+row.name+"</a></li>";
           }
           
           $("#attributeList").html(html);
        },
        'json'
    );
}

/**
 * Decides whether to add or remove a category based on its current state
 */
function toggleCategory(id) {
    if($.inArray(id, categoryEntries) > -1) {
        removeCategory(id);
    } else {
        addCategory(id);
    }
}

/**
 * Adds a category to the filter accordian
 */
function addCategory(id) {
    
    if($.inArray(id, categoryEntries) > -1) {
        return;
    }
    
    var $column = $('#leftColumn');
    
    $("#category-entry-"+id).addClass('selectedAttr');
    
    // TODO: Handle error states for this better, and render the select/text entry options
    $.get(
        serverURL,
        {"action":"getCategoryFilter","id":id},
        function(result) {
            if(!verifyResults(result))
                return;
            if(result.value == null) {
                alert("ERROR: No results.");
            }
           
            $column.prepend(drawFilterContents(result.value));
            
            $column.accordion('destroy').accordion({active: 0});

            // Add an entry to the array to track what we have
            categoryEntries[categoryEntries.length] = id;
            
        },
        "json"
    );
}

/**
 * Removes a category from the filter accordian
 */
function removeCategory(id) {
    var $column = $('#leftColumn');
    // Save which box was open
    var open = $column.accordion('option', 'active');
    $column.accordion('destroy');
    
    // Get the index of the entry to remove
    var index = $.inArray(id, categoryEntries);
    // Remove the entry from the array
    categoryEntries.splice(index, 1);
    // Remove it from the actual page (HACK!)
    
    // Fix index since elements are listed in reverse order
    index = categoryEntries.length - index;
    
    // Get all of the entries
    var entries = $column.children().filter("h3");
    // Grab the entry by the index
    var entry = $(entries[index]);
    
    // Actually remove the element
    entry.next().remove(); // next() is a jQuery function, not the default one, so it will work in IE
    entry.remove();
    
    if(open > index) {
        open--;
    }
    
    if(open > 0) {
        $column.accordion({active: open});
    } else {
        $column.accordion();
    }
    
    $("#category-entry-"+id).removeClass('selectedAttr');
}

/**
 * Called when a category value is changed (updates the results)
 */
function categoryChange() {
    $.get(serverURL,
        "action=updateResults&"+serializeFilterContents(),
        function(result) {
           if(!verifyResults(result))
               return;
           if(result.value == null) {
               alert("ERROR: No results.");
           }
           
           resultEntries = result.value;
           renderResult();
        },
        "json"
    );
}

// -- Result Functions -- //
/**
 * Actually renders the result column
 */
function renderResult() {
    // Assuming entries to follow the following scheme
    // {id:int, familyName:string, genusName:string, speciesName:string, commonName:["string", "string"], pictures:["string", "string"]}
    var $table = $("#resultTable");
    
    var html = "";
    
    for(var i = 0; i < resultEntries.length; i++) {
        var entry = resultEntries[i];
        
        html += "<tr><td>";
        
        html += "<div class='nameList'>"
//        if(entry.hasOwnProperty("familyName")) {
//            html += "<b>Family: </b><i>"+entry.familyName+"</i> ";
//        }
//        if(entry.hasOwnProperty("genusName")) {
//            html += "<b>Genus: </b><i>"+entry.genusName+"</i> ";
//        }
//        if(entry.hasOwnProperty("speciesName")) {
//            html += "<b>Species: </b><i>"+entry.speciesName+"</i> ";
//        }
        if(entry.hasOwnProperty("scientificName")) {
            html += "<b>Scientific Name: </b><i>"+entry.scientificName+"</i>";
        }
        
        if(entry.hasOwnProperty("commonName")) {
            html += "<br><b>Common Name(s): </b>";
            for(var j = 0; j < entry.commonName.length; j++) {
                html += entry.commonName[j];
                if(j < (entry.commonName.length-1)) {
                    html += ", ";
                }
            }
        }
        html += "</div>"
        
        if(entry.hasOwnProperty("pictures")) {
            html += "<div class='pictureList'>";
            for(var k = 0; k < entry.pictures.length; k++) {
                html += "<img height='120' src='"+entry.pictures[k]+"'>";
            }
            html += "</div>";
        }
        
        html += "<a href='#' onClick='openInfo("+entry.id+"); return false;'>See Full Info&gt;</a>";
        
        html += "</td></tr>";
    }
    
    if(resultEntries.length < 1) {
        html = "<tr><td>No results to display.</td></tr>";
    }

    $table.html(html);
}

/**
 * Returns a string with the generated contents for the category filter box
 */
function drawFilterContents(entry) {
    var html = "<h3><a href='#'>"+entry.name+"</a></h3>"+
        "<div id='category-filter-"+entry.id+"'>"+
        "<div style='float: right;'>[<a href='#' onClick='removeCategory("+entry.id+"); return false;'>Remove</a>]</div>"+
        entry.text + "<br>";
    switch(entry.type) {
        case 'text':
            // TODO: Add onBlur (or possibly onChange) hook  to link to something to update the results
            html += "<input type='text' name='categoryFilterForm-"+entry.id+"'>";
            break;
        case 'drop down':
            html += "<select name='categoryFilterForm-"+entry.id+"'>";
            html += "<option value='' disabled='disabled' selected='selected'>-- Choose --</option>"
            if(typeof entry.options != "undefined") {
                for(var i = 0; i < entry.options.length; i++) {
                    html += "<option value='"+entry.options[i][1]+"'>"+entry.options[i][1]+"</option>";
                }
            }
            html += "</select>";
            break;
        default:
            html += "<br><strong>Unable to determine what type this is. Please contact someone who can fix this.</strong>";
            break;
    }
    
    html += "</div>";
    
    return html;
}

function returnNameSearchResults(searchText) {
	$.get(
		serverURL,
		{"action":"getSearchByNameResults", "searchText":searchText},
		function(result) {
            if(!verifyResults(result))
               return;
			if(result.value == null) {
               alert("ERROR: No results.");
             }
 			
			resultEntries = result.value;  
			renderResult();  
           },
           "json"
	);
}

function serializeFilterContents() {
    return $("#filterForm").serialize();
}

// -- Result Pane Control functions -- //
function openInfo(id) {
    $("#infoContent").attr("src", "viewPlant.php?plantID="+id);
    $("#rightColumnContent").hide("slide", {direction:"left"}, 500);
    $("#rightColumnInfo").show("slide", {direction:"right"}, 500);
}

function openResults() {
    $("#rightColumnInfo").hide("slide", {direction:"right"}, 500);
    $("#rightColumnContent").show("slide", {direction:"left"}, 500);
}
