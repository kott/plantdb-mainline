<?php
	include 'config.php';
	include 'PlantClass.php';
	//Get values from text boxes in plantEntry.php
	$sciName = $_POST["scientificName"];
	$family = $_POST["family"];
	$genus = $_POST["genus"];
	$notes = $_POST["Notes"];
	$desc = $_POST["description"];
	$plantNames = $_POST["commonNames"];
	$numCategory = $_POST["numCategories"];
	$plantId = $_POST["plantID"];
	
	//Delete category values from db
	$stmt = mysqli_prepare($mysqli, "DELETE FROM test.plantcategories WHERE plant_id = ?");
	$stmt->bind_param('i', $plantId);
	$stmt->execute();
	$stmt->close();
	
	//Delete common names from db
	$stmt = mysqli_prepare($mysqli, "DELETE FROM test.common_names WHERE plant_id = ?");
	$stmt->bind_param('i', $plantId);
	$stmt->execute();
	$stmt->close();
	
	$plantObj = new Plant($plantId);
	if ($plantObj->scientificName != $sciName)
	{
		$stmt = mysqli_prepare($mysqli, "UPDATE plants SET sci_name = ? WHERE plant_id = ?");
		$stmt->bind_param('si', $sciName, $plantId);
		$stmt->execute();
		$stmt->close();
	}
	if ($plantObj->family != $family)
	{
		$stmt = mysqli_prepare($mysqli, "UPDATE plants SET family = ? WHERE plant_id = ?");
		$stmt->bind_param('si', $family, $plantId);
		$stmt->execute();
		$stmt->close();
	}
	if ($plantObj->genus != $genus)
	{
		$stmt = mysqli_prepare($mysqli, "UPDATE plants SET genus = ? WHERE plant_id = ?");
		$stmt->bind_param('si', $genus, $plantId);
		$stmt->execute();
		$stmt->close();
	}
	if ($plantObj->notes != $notes)
	{
		$stmt = mysqli_prepare($mysqli, "UPDATE plants SET notes = ? WHERE plant_id = ?");
		$stmt->bind_param('si', $notes, $plantId);
		$stmt->execute();
		$stmt->close();
	}
	
	//Store common names in db
	foreach ($plantNames as $name)
	{
		if (!empty($name) || $name != '')
		{
			$stmt = mysqli_prepare($mysqli, "INSERT INTO test.common_names (name, plant_id) VALUES(?,?)");
			$stmt->bind_param('si', $name, $plantId);
			$stmt->execute();
		}	
	}
	
	for ($i = 1; $i <= $numCategory; $i++ )
	{
		$categoryValue = $_POST["category$i"];
		$categoryId = $_POST["categoryId$i"];
		if (!empty($categoryValue))
		{
			if (is_array($categoryValue))
			{
				foreach ($categoryValue as $value)
				{
					if (!empty($value) || $value != '')
					{
						//echo 'category_id is ' . $categoryId . 'value is ' . $value . '\n';	
						$stmt = mysqli_prepare($mysqli, "INSERT INTO plantcategories (plant_id, category_id, value) VALUES(?,?,?)");
						$stmt->bind_param('iis', $plantId, $categoryId, $value);
						$stmt->execute();
					}	
				}
			}
			else 
			{
				if ($categoryValue != '')	
				{
					echo 'category id is ' . $categoryId . 'value is ' . $categoryValue . '/n'; 	
					$stmt = mysqli_prepare($mysqli, "INSERT INTO plantcategories (plant_id, category_id, value) VALUES(?,?,?)");
					$stmt->bind_param('iis', $plantId, $categoryId, $categoryValue);
					$stmt->execute();
				}
			}
		}
	}
	
	if (($_FILES["file"]["type"] == "image/gif")
		|| ($_FILES["file"]["type"] == "image/jpeg")
		|| ($_FILES["file"]["type"] == "image/png")
		|| ($_FILES["file"]["type"] == "image/pjpeg"))
	{
		if ($_FILES["file"]["error"] > 0)
		{
			echo "Error uploading file: " . $_FILES["file"]["error"] . "<br/>";
		}
		else
		{
			if (file_exists("/photos/". $_FILES["file"]["name"]))
			{
				echo $_FILES["file"]["name"] . " already exists. ";
			}
			else
			{
				//store picture on server in pictures folder	
				move_uploaded_file($_FILES["file"]["tmp_name"], "C:/inetpub/wwwroot/plantdb/photos/". $_FILES["file"]["name"]);
				//store picture location in pictures table
				$pictureLoc = "/photos/" . $_FILES["file"]["name"];
				$stmt = mysqli_prepare($mysqli, "INSERT INTO pictures (plant_id, location, description) VALUES(?,?,?)");
				$stmt->bind_param('iss', $plantId, $pictureLoc, $desc);
				$stmt->execute();
			}
		}
	}
	else 
	{
		echo "Invalid file type.";
	}
	header('Location: editPlant.php?plantID=' . $plantId);
?>

